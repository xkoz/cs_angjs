/**
 * Created by Yegor on 21.05.14.
 */

(function(){
    var app = angular.module('gemStore', []);

    app.controller('StoreController' ,function(){
        this.products = gems;
    });

    app.controller('PanelController', function(){
        this.tab = 1; // set tab to description

        this.selectTab = function(setTab){
            this.tab = setTab;
        }

        this.isSelected = function(selectedTab){
            return this.tab === selectedTab;
        }
    });

    app.controller('ReviewController',function(){
        this.review = {};

        this.addReview = function(product){
            this.review.createdOn = Date.now();
            product.reviews.push(this.review);
            this.review = {};
        }

    });

    var gems = [
        {
            name : 'Laptop',
            price : 995,
            description : 'In publishing and graphic design, lorem ipsum is a placeholder text commonly used to ' +
             'demonstrate the graphic elements of a document or visual presentation, ' +
             'such as font, typography, and layout, by removing the distraction of meaningful content. ',
            canPurchase: true,
            soldOut: false,
            images: [
                "image/vaio.jpg"
            ],
            reviews: [
                {
                    stars: 5,
                    email: "kozlovich@gmail.com",
                    details: "This is awesome. Vaio is the best laptop on the world.",
                    createdOn: 1402138994
                },
                {
                    stars: 2,
                    email: "yegor@gmail.com",
                    details: "Bad!",
                    createdOn: 1402111194
                }
            ]
        },
        {
            name : 'iPhone',
            price : 579,
            description : 'In publishing and graphic design, lorem ipsum is a placeholder text commonly used to ' +
                'demonstrate the graphic elements of a document or visual presentation, ' +
                'such as font, typography, and layout, by removing the distraction of meaningful content. ',
            canPurchase: true,
            soldOut: false,
            images: [
                "image/iphone.jpg"
            ],
            reviews: [
                {
                    stars: 5,
                    email: "kozlovich@gmail.com",
                    details: "Good design",
                    createdOn: 1441138994
                }
            ]
        },
        {
            name : 'Tom-tom',
            price : 350,
            description : 'In publishing and graphic design, lorem ipsum is a placeholder text commonly used to ' +
                'demonstrate the graphic elements of a document or visual presentation, ' +
                'such as font, typography, and layout, by removing the distraction of meaningful content. ',
            canPurchase: true,
            soldOut: false,
            images: [
                "image/tomtom.jpg",
                "image/tomtom2.jpg"
            ],
            reviews: [
                {
                    stars: 5,
                    email: "kozlovich@gmail.com",
                    details: "This is awesome. Tomtom is the best navigator on the world.",
                    createdOn: 1802138884
                },
                {
                    stars: 3,
                    email: "yegor@gmail.com",
                    details: "So-so!",
                    createdOn: 1495638994
                }
            ]
        }
    ]
}
)();